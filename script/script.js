var btnApurar = document.getElementById("idApurar");
var tabela = document.getElementById("idTabela");

tabela.classList.add("visually-hidden");

// Carregar a requisição ao carregar a tela
window.onload = async function () {
    await buscarVotos();
    await buscarVotosNA();
}

// Requisição para o backend
async function buscarVotos() {
    let response = await fetch("http://localhost:3001/apuracao");

    return response.json();
}

async function buscarVotosNA(){
    let response = await fetch("http://localhost:3001/apuracaoidentificada");

    return response.json();
}

// Apuração dos Votos 
btnApurar.onclick = async function(){
    var radioApuracao = document.querySelector("input[name=radio]:checked").value;

    document.getElementById("idBtnApurar").classList.add("visually-hidden");
    document.getElementById("idDivRadio").classList.add("visually-hidden");

    tabela.classList.remove("visually-hidden");
    var votacao;

    switch (radioApuracao) {
        case "a":
            votacao = await buscarVotos();
            break;
        case "na":
            votacao = await buscarVotosNA();
            break;
    }

    montarTabela(votacao);
}

// Montagem da tabela do resultado
function montarTabela(votacao){
        let tabelaBody = document.getElementById("idBodyTabela");

        for (let i = 0; i < votacao.length; i++) {
            let tabelaTr = document.createElement("tr");
            tabelaBody.appendChild(tabelaTr);

            tabelaTr.appendChild(montaTd(votacao[i][0]))
            tabelaTr.appendChild(montaTd(votacao[i][1]))
            tabelaTr.appendChild(montaTd(votacao[i][3])) 
        }
}
    
function montaTd(dado) {
    let tabelaTd = document.createElement("td");
    tabelaTd.textContent = dado;
    
    return tabelaTd
}
